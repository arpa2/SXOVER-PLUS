# Installation of the SASL mechanism SXOVER-PLUS

> *How to get it going, building, distributing.*

## Building from Source

This is a CMake project.  When all dependencies are installed,
you can build it with a sequence like

```
git clone https://gitlab.com/arpa2/SXOVER-PLUS
mkdir build
cd build
cmake ../SXOVER-PLUS
make
make test
make install
```

## Build Options

You can use the `-D` form of the CMake commandline to add options.
The simplest way to explore those is interactively.  This is done
with `ccmake` instead of `cmake` in the command above.

The following options are currently available:

  * `SUPPORT_CYRUS_SASL2` builds a plugin module for Cyrus-SASL2.
  * `SUPPORT_FREEDIAMETER` builds a Quick-DiaSASL local server that relays SASL over Diameter.

## ARPA2 Stack Dependencies

Before you can do this, there are a few dependencies, all available
as CMake projects that build like the instructions above:

  * [ARPA2CM](https://gitlab.com/arpa2/arpa2cm) is our CMake script collection
  * [ARPA2 Common](https://gitlab.com/arpa2/arpa2common) provides our core libraries
  * [Quick-MEM](https://gitlab.com/arpa2/Quick-MEM/) is our pooled memory library
  * [Quick-DER](https://gitlab.com/arpa2/quick-der) is our ASN.1 and DER handling library (for the protocol packages)
    with includes for ARPA2 protocols and many RFC-based ones
  * [Quick-SASL](https://gitlab.com/arpa2/Quick-SASL/) is our simplified SASL wrapper
  * [KIP](https://gitlab.com/arpa2/kip) provides the end-to-end encryption tunnel

You should not need freeDiameter to build the `SXOVER-PLUS` project itself,
but the contributed extension for freeDiameter needs it, obviously.

We have our own build environment named "mkhere", for which
[startup instructions](https://gitlab.com/arpa2/mkhere)
and
[this package script](https://gitlab.com/arpa2/mkhere/-/blob/master/sxoverplus.sh)
handle all dependencies and building as a wrapper around the CMake process.
This strategy is beneficial for us as part of the "mkroot" scripting
that we use for
[building Open Containers](https://gitlab.com/arpa2/mkroot)
that can be used with RunC and its successors.

## Operating System Dependencies

The following packages are commonly available.  We generally build on
Debian stable, Ubuntu and FreeBSD, of which the first is the platform
that we actively test on.

Required for this package and its ARPA2 Stack Dependencies:

  * `libev-dev`
  * `ragel`
  * `bison`
  * `flex`
  * `swig`
  * `liblmdb-dev`
  * `comerr-dev`
  * `libssl-dev`
  * `sasl2-bin`
  * `libsasl2-dev`
  * `libsasl2-modules`
  * `libsasl2-modules-db`
  * `libkrb5-dev`
  * `libfcgi`
  * `libfcgi-dev`
  * `libjson-c-dev`
  * `libunbound-dev`
  * `libssl-dev`
  * `libpcre3-dev`
  * `libjson-c-dev`
  * `python3`
  * `python3-dev`
  * `python3-cbor`
  * `python3-setuptools`

Advised for document building:

  * `doxygen`
  * `graphviz`

