/* SXOVER-PLUS -- Simple Authentication with Realm Crossover.
 *
 * This wraps an end-to-end encryption tunnel around a simpler
 * SASL mechanism.  The tunnel key is derived with KIP and
 * the wrapping is encoded/decoded with Quick DER.  The inner
 * SASL mechanism is implemented with Quick-SASL.
 *
 * This particular form of the mechanism builds a plugin for
 * the dominant Cyrus-SASL2 library for SASL.  After installing,
 * the mechanism should be available.  There is code for both
 * client and server sides of SXOVER-PLUS.  Backgrounds are on
 * https://www.cyrusimap.org/sasl/sasl/developer/plugprog.html
 *
 * This logic builds on libxoversasl.so but it could also be
 * linked to libxoversasl.o to avoid library dependencies in
 * the SASL plugin.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020-2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <time.h>

#include <sasl/sasl.h>
#include <sasl/saslplug.h>
#include <sasl/saslutil.h>

#include <arpa2/kip.h>

#include <arpa2/quick-der.h>
#include <quick-der/KIP-Service.h>
#include <quick-der/SASL-RealmCrossover.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include "arpa2/xover-sasl.h"


#define SXOVER_NAME  "SXOVER-PLUS"
#define SXOVER_LEN   (strlen (SXOVER_NAME))



/*
 ********** CLIENT CALLBACK CODE **********
 */



/* Client callbacks can be used to obtain identifying information.
 *
 *  - SASL_CB_GETREALM to obtain the client realm
 *    defaults to KIPSERVICE_CLIENT_REALM which is
 *    the general setting for the client identity provider
 *
 *  - SASL_CB_USER to obtain the client user name
 *    defaults to KIPSERVICE_CLIENTUSER_LOGIN which is
 *    the general setting for the client identity provider
 *
 *  - SASL_CB_AUTHNAME to obtain a client actor name (group, alias)
 *    defaults to KIPSERVICE_CLIENTUSER_ACL which is
 *    the general setting for the client identity provider
 *    and if not entered will be considered equal to SASL_CB_USER
 *
 * If all these environment variables are provided, the callback will
 * not be made.  Otherwise, the callback is made and values setup as
 * these environment variables for the course of this process.  Any
 * values present will be presented as default values and may then be
 * overridden for the duration of this process.
 *
 * Note: The foreign server realm does plays no role in SXOVER-PLUS,
 * because it merely serves as a proxy for the SASL tokens.  At most
 * could there be an ACL on the identity provider to choose whether
 * the foreign server is welcomed to login, in which case it plays a
 * role in the Diameter link for Realm Crossover alone.
 */



/*
 ********** SERVER CONFIGURATION CODE **********
 */



/* Server configuration is extracted from a SASL configuration file.
 *
 * SXOVER-PLUS uses "InternetWide_Identity" as its application name, so
 * the corresponding configuration file is found at
 * "${SASL_CONF_PATH:-/etc/sasl2}/InternetWide_Identity.conf".
 *
 * In this file, options can be defined for the connection to the
 * local KIP Daemon, as it is used by the client to wrap the login
 * attempt into an end-to-end encrypted tunnel:
 *
 *  - kipservice-idp-domain <domain>
 *    Defines a domain that leads to the KIP server for that domain
 *    (and possibly others, in the case of virtual hosting).
 *
 *  - kipservice-idp-login <user name>
 *    Defines a user name for the identity provider's login to this
 *    KIP service.  Login is required so identity provisioning is a
 *    protected role.
 *
 *  - kipservice-idp-secret <passtoken>
 *    For SASL mechanisms using mere password/token access, this is
 *    the definition of the password/token to use.  Since this is an
 *    internal security link, and since any automatically started
 *    identity provider would initially need a passphrase somewhere,
 *    this may often be the only available option.  On platforms
 *    that can securely provide environment variables, such as some
 *    virtual platforms, set $QUICKSASL_PASSPHRASE instead of using
 *    this configuration file option.
 *
 * This configuration setup is stored in environment variables that
 * are active for the duration of this process:
 *
 *  - KIPSERVICE_CLIENT_REALM      (set to kipservice-domain)
 *  - KIPSERVICE_CLIENTUSER_LOGIN  (set to kipservice-idp-login)
 *  - KIPSERVICE_CLIENTUSER_ACCESS (removed)
 *  - QUICKSASL_PASSPHRASE         (set to kipservice-idp-secret)
 */



/*
 ********** LIBRARY WRAPPERS **********
 */



/* client: create context for mechanism, using params supplied
 *  glob_context   -- from above
 *  params         -- params from sasl_client_new
 *  conn_context   -- context for one connection
 * returns:
 *  SASL_OK        -- success
 *  SASL_NOMEM     -- not enough memory
 *  SASL_WRONGMECH -- mech doesn't support security params
 */
int sxover_cli_new (void *glob_context,
				sasl_client_params_t *cparams,
				void **conn_context) {
	/* Construct a XoverSASL client */
	if (!xsasl_client ((XoverSASL *) conn_context, cparams->service, NULL, 0, 0)) {
		return SASL_WRONGMECH;
	}
	/* Preselect the SXOVER-PLUS mechanism */
	xsasl_fixate_mech ((XoverSASL) conn_context);
	/* Success */
	return SASL_OK;
}


/* server: create a new mechanism handler
 *  glob_context  -- global context
 *  sparams       -- server config params
 *  challenge     -- server challenge from previous instance or NULL
 *  challen       -- length of challenge from previous instance or 0
 * out:
 *  conn_context  -- connection context
 *  errinfo       -- error information
 *
 * returns:
 *  SASL_OK       -- successfully created mech instance
 *  SASL_*        -- any other server error code
 */
int sxover_srv_new (void *glob_context,
				sasl_server_params_t *sparams,
				const char *challenge,
				unsigned challen,
				void **conn_context) {
	/* Construct a XoverSASL server */
	if (!xsasl_server ((XoverSASL *) conn_context, sparams->service, NULL, 0, QSA_SERVERFLAG_ALLOW_FINAL_S2C)) {
		return SASL_WRONGMECH;
	}
	/* Preselect the SXOVER-PLUS mechanism */
	xsasl_fixate_mech ((XoverSASL) conn_context);
	/* Success */
	return SASL_OK;
}


/* client: dispose of connection context from mech_new
 */
void sxover_cli_dispose (void *conn_context,
				const sasl_utils_t *utils) {
	xsasl_close ((XoverSASL *) &conn_context);
}


/* server: dispose of a connection state
 */
void sxover_srv_dispose (void *conn_context,
				const sasl_utils_t *utils) {
	xsasl_close ((XoverSASL *) &conn_context);
}


/* client/server: free global state for mechanism
 *  mech_dispose must be called on all mechanisms first
 */
void sxover_cli_srv_free (void *glob_context,
				const sasl_utils_t *utils) {
	xsasl_fini ();
	kipservice_fini ();
	kip_fini ();
}


/* server: perform one step of the SXOVER-PLUS exchange.
 *
 * returns:
 *  SASL_OK       -- success, all done
 *  SASL_CONTINUE -- success, one more round trip
 *  SASL_*        -- any other server error code
 */
int sxover_srv_step (void *conn_context,
				sasl_server_params_t *sparams,
				const char *clientin,
				unsigned clientinlen,
				const char **serverout,
				unsigned *serveroutlen,
				sasl_out_params_t *oparams) {
	XoverSASL xs = (XoverSASL) conn_context;
	dercursor c2s;
	c2s.derptr = (uint8_t *) clientin;
	c2s.derlen =             clientinlen;
	dercursor out_s2c;
	if (!xsasl_step_server (xs, c2s, &out_s2c)) {
		return SASL_FAIL;
	}
	bool success = false;
	bool failure = true;
	if (!qsasl_get_outcome (xs, &success, &failure)) {
		return SASL_FAIL;
	}
	if (success && failure) {
		return SASL_FAIL;
	}
	*serverout    = out_s2c.derptr;
	*serveroutlen = out_s2c.derlen;
	if (success) {
		return SASL_OK;
	} else if (failure) {
		return SASL_BADAUTH;
	} else {
		return SASL_CONTINUE;
	}
}


/* perform one step of the SXOVER-PLUS exchange.
 * NULL is passed for serverin on the first step.
 *
 * returns:
 *  SASL_OK        -- success
 *  SASL_INTERACT  -- user interaction needed to fill in prompts
 *  SASL_BADPROT   -- server protocol incorrect/cancelled
 *  SASL_BADSERV   -- server failed mutual auth
 */
int sxover_cli_step (void *conn_context,
				sasl_client_params_t *cparams,
				const char *serverin,
				unsigned serverinlen,
				sasl_interact_t **prompt_need,
				const char **clientout,
				unsigned *clientoutlen,
				sasl_out_params_t *oparams) {
	XoverSASL xs = (XoverSASL) conn_context;
	/* First round, consider interaction with the user */
	if (serverin == NULL) {
		/* Split the call before interaction... */
		if (prompt_need == NULL) {
			/* Interact on missing environment variables */
			const char *realm = getenv ("KIPSERVICE_CLIENT_REALM");
			const char *login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
			const char *acces = getenv ("KIPSERVICE_CLIENTUSER_ACL");
			if ((realm == NULL) || (login == NULL) || (acces == NULL)) {
				/* Construct user interaction */
				sasl_interact_t *ia = NULL;
				if (!mem_alloc (xs, 3 * sizeof (sasl_interact_t), (void **) &ia)) {
					return SASL_NOMEM;
				}
				/* Fill the realm, login, acces fields */
				ia [0].id = SASL_CB_GETREALM;
				ia [1].id = SASL_CB_USER;
				ia [2].id = SASL_CB_AUTHNAME;
				ia [0].prompt = "Bring Your Own Domain: ";
				ia [1].prompt = "Your login under that domain: ";
				ia [2].prompt = "Optionally acting as: ";
				ia [0].defresult = realm;
				ia [1].defresult = login;
				ia [2].defresult = acces ? acces : login;
				/* Give the user the opportunity */
				*prompt_need = ia;
				return SASL_INTERACT;
			}
		/* ...and the call after interaction */
		} else {
			sasl_interact_t *ia = *prompt_need;
			/* Harvest the resulting strings */
			unsigned realmlen = ia [0].len;
			unsigned loginlen = ia [1].len;
			unsigned acceslen = ia [2].len;
			const char *realm = (realmlen > 0) ? ia [0].result : NULL;
			const char *login = (loginlen > 0) ? ia [1].result : NULL;
			const char *acces = (acceslen > 0) ? ia [2].result : NULL;
			/* Test that the strings are workable */
			if ((realm == NULL) || (login == NULL)) {
				return SASL_BADPARAM;
			}
			char realmcpy [realmlen + 1];
			memcpy (realmcpy, realm, realmlen);
			realmcpy [realmlen] = '\0';
			setenv ("KIPSERVICE_REALM", realmcpy, 1);
			char logincpy [loginlen + 1];
			memcpy (logincpy, login, loginlen);
			logincpy [loginlen] = '\0';
			setenv ("KIPSERVICE_CLIENTUSER_LOGIN", logincpy, 1);
			if (acces != NULL) {
				char accescpy [acceslen + 1];
				memcpy (accescpy, acces, acceslen);
				accescpy [acceslen] = '\0';
				setenv ("KIPSERVICE_CLIENTUSER_ACL", accescpy, 1);
			} else {
				acces = login;
				unsetenv ("KIPSERVICE_CLIENTUSER_ACL");
			}
		}
	}
	/* We now have acceptable KIPSERVICE_xxx environment variables */
	dercursor s2c;
	s2c.derptr = (uint8_t *) serverin;
	s2c.derlen =             serverinlen;
	dercursor out_mech;
	dercursor out_c2s;
	if (!xsasl_step_client (xs, s2c, &out_mech, &out_c2s)) {
		return SASL_FAIL;
	}
	bool success = false;
	bool failure = true;
	if (!qsasl_get_outcome (xs, &success, &failure)) {
		return SASL_FAIL;
	}
	if (success && failure) {
		return SASL_FAIL;
	}
	*clientout    = out_c2s.derptr;
	*clientoutlen = out_c2s.derlen;
	if (success) {
		return SASL_OK;
	} else if (failure) {
		return SASL_BADAUTH;
	} else {
		return SASL_CONTINUE;
	}
}


/* The structure with the internal client API for Cyrus SASL2.
 */
static struct sasl_client_plug sxover_cli_plug_init = {
	.mech_name = SXOVER_NAME,
	.max_ssf = 128,
	.security_flags = SASL_SEC_NOPLAINTEXT | SASL_SEC_NOACTIVE | SASL_SEC_NODICTIONARY | SASL_SEC_NOANONYMOUS | SASL_SEC_PASS_CREDENTIALS | SASL_SEC_MUTUAL_AUTH,
	.features = SASL_FEAT_WANT_CLIENT_FIRST | SASL_FEAT_CHANNEL_BINDING,
	.mech_new = sxover_cli_new,
	.mech_step = sxover_cli_step,
	.mech_dispose = sxover_cli_dispose,
	.mech_free = sxover_cli_srv_free,
};


/* The structure with the internal server API for Cyrus SASL2.
 */
static struct sasl_server_plug sxover_srv_plug_init = {
	.mech_name = SXOVER_NAME,
	.max_ssf = 128,
	.security_flags = SASL_SEC_NOPLAINTEXT | SASL_SEC_NOACTIVE | SASL_SEC_NODICTIONARY | SASL_SEC_NOANONYMOUS | SASL_SEC_PASS_CREDENTIALS | SASL_SEC_MUTUAL_AUTH,
	.features = SASL_FEAT_WANT_CLIENT_FIRST | SASL_FEAT_CHANNEL_BINDING,
	.mech_new = sxover_srv_new,
	.mech_step = sxover_srv_step,
	.mech_dispose = sxover_srv_dispose,
	.mech_free = sxover_cli_srv_free,
};


#ifdef WIN32
#define PLUG_API __declspec(dllexport)
#else
#define PLUG_API extern
#endif


/* plug-in entry point:
 *  utils       -- utility callback functions
 *  max_version -- highest client plug version supported
 * returns:
 *  out_version -- client plug version of result
 *  pluglist    -- list of mechanism plug-ins
 *  plugcount   -- number of mechanism plug-ins
 * results:
 *  SASL_OK       -- success
 *  SASL_NOMEM    -- failure
 *  SASL_BADVERS  -- max_version too small
 *  SASL_BADPARAM -- bad config string
 *  ...
 */
PLUG_API int sasl_client_plug_init (const sasl_utils_t *utils,
				    int max_version,
				    int *out_version,
				    sasl_client_plug_t **pluglist,
				    int *plugcount) {
	if (SASL_CLIENT_PLUG_VERSION > max_version) {
		return SASL_BADVERS;
	} else if (!kip_init ()) {
		return SASL_NOMEM;
	} else {
		/* Setup the structure */
		*out_version = SASL_CLIENT_PLUG_VERSION;
		*pluglist = &sxover_cli_plug_init;
		*plugcount = 1;
		/* Return success */
		return SASL_OK;
	}
}


/* plug-in entry point:
 *  utils         -- utility callback functions
 *  plugname      -- name of plug-in (may be NULL)
 *  max_version   -- highest server plug version supported
 * returns:
 *  out_version   -- server plug-in version of result
 *  pluglist      -- list of mechanism plug-ins
 *  plugcount     -- number of mechanism plug-ins
 * results:
 *  SASL_OK       -- success
 *  SASL_NOMEM    -- failure
 *  SASL_BADVERS  -- max_version too small
 *  SASL_BADPARAM -- bad config string
 *  ...
 */
PLUG_API int sasl_server_plug_init (const sasl_utils_t *utils,
					int max_version,
					int *out_version,
					sasl_server_plug_t **pluglist,
					int *plugcount) {
	if (SASL_SERVER_PLUG_VERSION > max_version) {
		return SASL_BADVERS;
	} else if (!kip_init ()) {
		return SASL_NOMEM;
	} else if (!kipservice_init (NULL, NULL)) {
		kip_fini ();
		return SASL_NOMEM;
	} else if (!xsasl_init (NULL, "InternetWide_Identity")) {
		kipservice_fini ();
		kip_fini ();
		return SASL_NOMEM;
	} else {
		/* Setup any environment variables as specified */
		if (utils->getopt != NULL) {
			const char *optstr;
			unsigned    optlen;
			/* kipservice-idp-domain --> $KIPSERVICE_REALM */
			if ((utils->getopt (utils->getopt_context,
					NULL,
					"kipservice-idp-domain",
					&optstr, &optlen) == SASL_OK)) {
				char optcpy [optlen + 1];
				memcpy (optcpy, optstr, optlen);
				optcpy [optlen] = '\0';
				setenv ("KIPSERVICE_REALM", optcpy, 1);
			}
			/* kipservice-idp-login --> $KIPSERVICE_CLIENTUSER_LOGIN */
			if ((utils->getopt (utils->getopt_context,
					NULL,
					"kipservice-idp-login",
					&optstr, &optlen) == SASL_OK)) {
				char optcpy [optlen + 1];
				memcpy (optcpy, optstr, optlen);
				optcpy [optlen] = '\0';
				setenv ("KIPSERVICE_CLIENTUSER_LOGIN", optcpy, 1);
				/* Extra: erase $KIPSERVICE_CLIENTUSER_ACCESS */
				unsetenv ("KIPSERVICE_CLIENTUSER_ACCESS");
			}
			/* kipservice-idp-secret --> $QUICKSASL_PASSPHRASE */
			if ((utils->getopt (utils->getopt_context,
					NULL,
					"kipservice-idp-secret",
					&optstr, &optlen) == SASL_OK)) {
				char optcpy [optlen + 1];
				memcpy (optcpy, optstr, optlen);
				optcpy [optlen] = '\0';
				setenv ("QUICKSASL_PASSPHRASE", optcpy, 1);
			}
		}
		/* Setup the structure */
		*out_version = SASL_SERVER_PLUG_VERSION;
		*pluglist = &sxover_srv_plug_init;
		*plugcount = 1;
		/* Return success */
		return SASL_OK;
	}
}

