
add_library (libsxoverplus_shared SHARED
	sxoverplus.c
)

set_target_properties (libsxoverplus_shared
	PROPERTIES OUTPUT_NAME sxoverplus)

target_link_libraries (libsxoverplus_shared
    # ${Cyrus-SASL2_LIBRARIES}
    XoverSASL
)

include (FindPkgConfig)
pkg_check_modules (CYRUS_SASL2 REQUIRED libsasl2)

install (TARGETS libsxoverplus_shared
        LIBRARY DESTINATION "${CYRUS_SASL2_LIBDIR}/sasl2")

