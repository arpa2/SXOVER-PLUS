/* diasasl_app.h -- freeDiameter Extension app for Diameter-SASL
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/* Header file for the diasasl extension. 
 *
 *  This extension provides a way to send configurable messages on the network
 *
 */
 
#include <freeDiameter/extension.h>

/* Mode for the extension */
#define MODE_SERV	0x1
#define	MODE_CLI	0x2

/* The module configuration */
struct diasasl_conf {
	uint32_t	vendor_id;	/* default 999999 */
	uint32_t	appli_id;	/* default 0xffffff */
	uint32_t	cmd_id;		/* default 0xfffffe */
	uint32_t	avp_id;		/* default 0xffffff */
	uint32_t	long_avp_id;	/* default 0 */
	size_t		long_avp_len;	/* default 5000 */
	int		mode;		/* default MODE_SERV | MODE_CLI */
	char 	*	dest_realm;	/* default local realm */
	char 	*	dest_host;	/* default NULL */
	char 	*	user_name;	/* default NULL */
	char 	*	diasasl_ip;	/* default 0.0.0.0 */
	int 		diasasl_port; /* default 12346 */
	char	*	sasl_app;	/* default "InternetWide Identity" */
	struct diasasl_stats {
		unsigned long long	nb_echoed; /* server */
		unsigned long long	nb_sent;   /* client */
		unsigned long long	nb_recv;   /* client */
		unsigned long long	nb_errs;   /* client */
		unsigned long		shortest;  /* fastest answer, in microseconds */
		unsigned long		longest;   /* slowest answer, in microseconds */
		unsigned long		avg;       /* average answer time, in microseconds */
	} stats;
	pthread_mutex_t		stats_lock;
};
extern struct diasasl_conf * diasasl_conf;

/* Parse the configuration file */
int diasasl_conf_handle(char * conffile);

/* Handle incoming messages (server) */
int diasasl_serv_init(void);
void diasasl_serv_fini(void);

/* Create outgoing message (client) */
int diasasl_cli_init(void);
void diasasl_cli_fini(void);

/* Initialize dictionary definitions */
int diasasl_dict_init(void);

void *diasasl_socket(void *arg);
void *diasasl_socket_der(void *arg);

/* Some global variables for dictionary */
extern struct dict_object * diasasl_vendor;
extern struct dict_object * diasasl_appli;
extern struct dict_object * diasasl_cmd_r;
extern struct dict_object * diasasl_cmd_a;

extern struct dict_object * diasasl_sess_id;
extern struct dict_object * diasasl_origin_host;
extern struct dict_object * diasasl_origin_realm;
extern struct dict_object * diasasl_dest_host;
extern struct dict_object * diasasl_dest_realm;
extern struct dict_object * diasasl_user_name;
extern struct dict_object * diasasl_res_code;
extern struct dict_object * diasasl_avp_sasl_mechanism;
extern struct dict_object * diasasl_avp_sasl_token;
extern struct dict_object * diasasl_avp_sasl_channel_binding;
