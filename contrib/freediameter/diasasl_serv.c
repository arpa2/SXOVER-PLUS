/* diasasl_serv.c -- freeDiameter Extension app/server for Diameter-SASL
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/* Install the dispatch callbacks */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/kip.h>
#include <arpa2/quick-der.h>
#include <arpa2/xover-sasl.h>

#include "diasasl_app.h"

#include <sasl/sasl.h>

#include "sasl-common.c"

static struct disp_hdl * diasasl_hdl_fb = NULL; /* handler for fallback cb */
static struct disp_hdl * diasasl_hdl_tr = NULL; /* handler for Test-Request req cb */
static xsaslt_state curstate;
static char *service;

static struct session_handler * sess_handler = NULL;

struct sess_state {
	XoverSASL sasl;
} ;

/* Default callback for the application. */
static int diasasl_fb_cb( struct msg ** msg, struct avp * avp, struct session * sess, void * opaque, enum disp_action * act)
{
	/* This CB should never be called */
	TRACE_ENTRY("%p %p %p %p", msg, avp, sess, act);
	
	fd_log_debug("Unexpected message received!");
	
	return ENOTSUP;
}

static void set_realms(XoverSASL sasl, struct avp_hdr *origin_realm_hdr, struct avp_hdr *dest_realm_hdr)
{
	if (origin_realm_hdr) {
		membuf buf_client_realm = {
			.bufptr = origin_realm_hdr->avp_value->os.data,
			.buflen = origin_realm_hdr->avp_value->os.len
		};
		assert (xsasl_set_client_realm     (sasl, buf_client_realm));
	}
	if (dest_realm_hdr) {
		membuf buf_server_realm = {
			.bufptr = dest_realm_hdr->avp_value->os.data,
			.buflen = dest_realm_hdr->avp_value->os.len
		};
		assert (xsasl_set_server_realm     (sasl, buf_server_realm));
	}
}

static XoverSASL get_sasl(struct session * sess, struct avp_hdr *origin_realm_hdr, struct avp_hdr *dest_realm_hdr)
{
	XoverSASL sasl = NULL;
	struct sess_state *mi = NULL;
	/* Search the session, retrieve its data */
	fd_log_debug("retrieving session state");
	/* retrieve this value in the session, AFTERWARDS it is removed from the list! */
	CHECK_FCT_DO( fd_sess_state_retrieve( sess_handler, sess, &mi ), goto out );
	fd_log_debug("retrieved session state: %p", mi);
	if (mi == NULL) {
		fd_log_debug("sasl: mi is NULL");
		assert (xsasl_server (&sasl, service, NULL, NULL, QSA_SERVERFLAG_ALLOW_FINAL_S2C));
		fd_log_debug("XSASL_SERVER sasl: %p", sasl);

		mi = malloc(sizeof(struct sess_state));
		if (mi == NULL) {
			fd_log_debug("malloc failed: %s", strerror(errno));
			goto out;
		}
		mi->sasl = sasl;
		set_realms(sasl, origin_realm_hdr, dest_realm_hdr);
	} else {
		sasl = mi->sasl;
		fd_log_debug("sasl: %p", sasl);
	}
	/* (re)store this value in the session, AFTERWARDS mi is set to NULL! (changed ownership) */
	CHECK_FCT_DO( fd_sess_state_store ( sess_handler, sess, &mi ), goto out );
out:
	return sasl;
}

/* Callback for incoming Test-Request messages */
static int diasasl_tr_cb( struct msg ** msg, struct avp * avp, struct session * sess, void * opaque, enum disp_action * act)
{
	struct msg *ans, *qry;
	struct avp *sasl_mechanism;
	struct avp *sasl_token;
	struct avp *sasl_channel_binding;
	struct avp_hdr *dest_realm_hdr = NULL;
	
	fd_log_debug("ENTRY: %p %p %p %p", msg, avp, sess, act);
	
	if (msg == NULL)
		return EINVAL;
	

	/* Value of Origin-Host */
	struct avp *origin_host;
	CHECK_FCT( fd_msg_search_avp ( *msg, diasasl_origin_host, &origin_host) );
	if (origin_host) {
		struct avp_hdr * hdr;
		CHECK_FCT( fd_msg_avp_hdr( origin_host, &hdr ) );
		fd_log_debug("Origin-Host: %.*s", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fd_log_debug("no_Origin-Host");
	}
	/* Value of Destination-Realm */
	struct avp *dest_realm;
	CHECK_FCT( fd_msg_search_avp ( *msg, diasasl_dest_realm, &dest_realm) );
	if (dest_realm) {
		CHECK_FCT( fd_msg_avp_hdr( dest_realm, &dest_realm_hdr ) );
		fd_log_debug("Destination-Realm: %.*s", (int)dest_realm_hdr->avp_value->os.len, dest_realm_hdr->avp_value->os.data);
	} else {
		fd_log_debug("no Destination-Realm");
	}
	fd_log_debug(", replying...\n");
	
	/* Create answer header */
	qry = *msg;
	CHECK_FCT( fd_msg_new_answer_from_req ( fd_g_config->cnf_dict, msg, 0 ) );
	ans = *msg;
	
	char *rescode = "DIAMETER_UNABLE_TO_COMPLY";
	CHECK_FCT_DO( fd_msg_search_avp ( qry, diasasl_avp_sasl_mechanism, &sasl_mechanism), goto out );
	CHECK_FCT_DO( fd_msg_search_avp ( qry, diasasl_avp_sasl_channel_binding, &sasl_channel_binding), goto out );
	CHECK_FCT_DO( fd_msg_search_avp ( qry, diasasl_avp_sasl_token, &sasl_token), goto out );

	XoverSASL sasl = NULL;
	int done = 0;
	/* Check the sasl_mechanism Payload AVP */
	if (sasl_mechanism) {
		//TODO// Error when also new_mechanism
		struct avp_hdr * hdr;

		sasl = get_sasl(sess, dest_realm_hdr, dest_realm_hdr);
		CHECK_FCT( fd_msg_avp_hdr( sasl_mechanism, &hdr )  );
		if (hdr->avp_value->os.len == 0) {
			//
			// Now provide a list of mechanisms for the client
			// Mechanism lists are not standardised but these demos pass them as
			// a string with spaces between the standardised mechanism names.
			dercursor csr_mechs;
			assert (xsasl_get_mech (sasl, &csr_mechs));
			union avp_value result = {
				.os = {
					.data = csr_mechs.derptr,
					.len = csr_mechs.derlen
				}
			};
			fd_log_debug("Sending mechs: %.*s", csr_mechs.derlen, csr_mechs.derptr);
			rescode = "DIAMETER_MULTI_ROUND_AUTH";
			CHECK_FCT( fd_msg_avp_new ( diasasl_avp_sasl_mechanism, 0, &avp ) );
			CHECK_FCT( fd_msg_avp_setvalue( avp, &result ) );
			CHECK_FCT( fd_msg_avp_add( ans, MSG_BRW_LAST_CHILD, avp ) );
			done = 1;
		} else {
			dercursor mech_used = {
				.derptr = hdr->avp_value->os.data,
				.derlen = hdr->avp_value->os.len
			};
			fd_log_debug("Found non-empty sasl_mechanism Payload AVP, mech: %.*s", mech_used.derlen, mech_used.derptr);

			rescode = "DIAMETER_MULTI_ROUND_AUTH";
			assert (xsasl_set_mech (sasl, mech_used));
		}
	} else {
		fd_log_debug("sasl_mechanism Payload AVP not present");
	}
	if (!done) {
		if (sasl_channel_binding || sasl_token) {
			if (sasl == NULL) {
				sasl = get_sasl(sess, dest_realm_hdr, dest_realm_hdr);
			}
			/* Check the SASL-Channel-Binding */
			if (sasl_channel_binding) {
				//TODO// Error when also new_mechanism
				struct avp_hdr * hdr;

				fd_log_debug("Found SASL-Channel-Binding AVP");

				CHECK_FCT( fd_msg_avp_hdr( sasl_channel_binding, &hdr )  );
				membuf chanbind = {
					.bufptr = hdr->avp_value->os.data,
					.buflen = hdr->avp_value->os.len
				};
				assert (xsasl_set_chanbind(sasl, false, chanbind));
			} else {
				fd_log_debug("SASL-Channel-Binding AVP *not* found");
			}

			/* Check the SASL-Token AVP */
			dercursor c2s;
			if (sasl_token != NULL) {
				//TODO// Error when also new_mechanism
				struct avp_hdr * hdr = NULL;
				CHECK_FCT( fd_msg_avp_hdr( sasl_token, &hdr )  );
				fd_log_debug("Found SASL-Token AVP");
				c2s.derptr = hdr->avp_value->os.data;
				c2s.derlen = hdr->avp_value->os.len;
			} else {
				fd_log_debug("SASL-Token AVP *not* found");
				c2s.derptr = NULL;
				c2s.derlen = 0;
			}
			// Make a server step: c2s -> s2c
			dercursor s2c;
			assert (xsasl_step_server (sasl, c2s, &s2c));
			bool success;
			bool failure;
			if (!xsasl_get_outcome(sasl, &success, &failure)) {
				fd_log_debug("error in xsasl_get_outcome");
				return -1;
			}
			fd_log_debug("xsasl_get_outcome: success: %d, failure: %d", success, failure);

			//
			// Process success/failure outcome
			if (failure) {
				//
				// Authentication failure
				rescode = "DIAMETER_AUTHENTICATION_REJECTED";
			} else {
				if (!der_isnull (&s2c)) {
					union avp_value result = {
						.os = {
							.data = s2c.derptr,
							.len = s2c.derlen
						}
					};
					CHECK_FCT( fd_msg_avp_new ( diasasl_avp_sasl_token, 0, &avp ) );
					CHECK_FCT( fd_msg_avp_setvalue( avp, &result ) );
					CHECK_FCT( fd_msg_avp_add( ans, MSG_BRW_LAST_CHILD, avp ) );
				}
				if (success) {
					//
					// Authentication success
					rescode = "DIAMETER_SUCCESS";
					dercursor userid;
					if (xsasl_get_client_userid (sasl, crs2bufp (&userid))) {
						if (userid.derptr == NULL) {
							fd_log_debug("No client identity\n");
						} else {
							fd_log_debug("Client identity is %.*s\n", userid.derlen, userid.derptr);
							union avp_value user_name = {
								.os = {
									.data = userid.derptr,
									.len  = userid.derlen
								}
							};
							CHECK_FCT( fd_msg_avp_new ( diasasl_user_name, 0, &avp ) );
							CHECK_FCT( fd_msg_avp_setvalue( avp, &user_name ) );
							CHECK_FCT( fd_msg_avp_add( ans, MSG_BRW_LAST_CHILD, avp ) );
						}
					}
					fd_sess_destroy(&sess);
					xsasl_close (&sasl);
				} else {
					//
					// Continued challenge/response exchange
					rescode = "DIAMETER_MULTI_ROUND_AUTH";
				}
			}
		}
	}
diasaslout:
	fd_log_debug("Result Code: %s", rescode);
	
	/* Set the Origin-Host, Origin-Realm, Result-Code AVPs */
	CHECK_FCT( fd_msg_rescode_set( ans, rescode, NULL, NULL, 1 ) );
	
	/* Send the answer */
	CHECK_FCT( fd_msg_send( msg, NULL, NULL ) );

	return 0;
out:
	return -1;
}

int diasasl_serv_init(void)
{
	struct disp_when data;
	
	service = "RealmCrossover";

	CHECK_FCT( fd_sess_handler_create(&sess_handler, (void *)free, NULL, NULL) );
	
	TRACE_DEBUG(FULL, "Initializing dispatch callbacks for test");
	
	memset(&data, 0, sizeof(data));
	data.app = diasasl_appli;
	data.command = diasasl_cmd_r;

	/* fallback CB if command != Test-Request received */
	CHECK_FCT( fd_disp_register( diasasl_fb_cb, DISP_HOW_APPID, &data, NULL, &diasasl_hdl_fb ) );
	
	/* Now specific handler for Test-Request */
	CHECK_FCT( fd_disp_register( diasasl_tr_cb, DISP_HOW_CC, &data, NULL, &diasasl_hdl_tr ) );
	
	//
	// Start the SASL session as a server.
	kip_init ();
	kipservice_init (NULL, NULL);
	xsasl_init (NULL, diasasl_conf->sasl_app);
	return 0;
}

void diasasl_serv_fini(void)
{
	if (diasasl_hdl_fb) {
		(void) fd_disp_unregister(&diasasl_hdl_fb, NULL);
	}
	if (diasasl_hdl_tr) {
		(void) fd_disp_unregister(&diasasl_hdl_tr, NULL);
	}

	//
	// Cleanup & Closedown
	xsasl_fini ();
	kipservice_fini ();
	kip_fini ();

	CHECK_FCT_DO( fd_sess_handler_destroy(&sess_handler, NULL), /* continue */ );

	return;
}
