/* diasasl_cli.c -- freeDiameter Extension app/client for Diameter-SASL
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2015, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/* Create and send a message, and receive it */

/* Note that we use both sessions and the argument to answer callback to pass the same value.
 * This is just for the purpose of checking everything went OK.
 */
#include "diasasl_app.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sasl/saslutil.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-dermem.h>
#include <arpa2/quick-diasasl.h>
#include <arpa2/except.h>

#include <quick-der/Quick-DiaSASL.h>

#include <unbound.h>

#define HOSTNAME_BUFSIZE 256


#define CL_IN     1
#define RR_SRV   33
#define RR_A      1
#define RR_AAAA  28

typedef int SOCKET;
typedef struct {
	char *mech;
	char *c2s;
	char *s2s;
} http_sasl_t;

struct sess_state {
	char dest_realm[HOSTNAME_BUFSIZE];
};

typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Request SASL_DIAMETER_AAR;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Answer        SASL_DIAMETER_AAA;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Request  SASL_DIAMETER_OPEN;
typedef DER_OVLY_Quick_DiaSASL_DiaSASL_Request       DER_Request;

typedef struct {
	int socket;
	char *dest_realm;
	DER_Request request;
} state_t;

static struct ub_ctx *ubctx = NULL;
static struct session_handler * sess_handler = NULL;

typedef uint16_t net_uint16_t;

static void diasasl_send_message_der_open(state_t *master_state, SASL_DIAMETER_OPEN *diameter_open);
static void diasasl_send_message_der(state_t *master_state);

/* Receive a request.
 */
bool recv_request (void *master, int cnx, DER_Request *req) {
	static const derwalk pack_der_request [] = {
		DER_PACK_Quick_DiaSASL_DiaSASL_Request,
		DER_PACK_END
	};
	return dermem_recv(master, cnx, pack_der_request, (struct dercursor *) req);
}


bool _dns2hostport (char *data, int datalen, char hostname [HOSTNAME_BUFSIZE], net_uint16_t *netport) {
	int hostofs = 0;
	char dataofs = 6;
	//
	// Harvest the port number as-is, that is, in network byte order
	if (datalen < 10) {
		/* Sillily small */
		return false;
	}
	* netport = * (uint16_t *) &data [4];
	//
	// Collect the labels of the host name
	while (data [dataofs] != 0) {
		unsigned int eltlen = data [dataofs];
		if (dataofs + eltlen + 1 > datalen) {
			return false;
		}
		if (hostofs + eltlen + 1 > HOSTNAME_BUFSIZE) {
			return false;
		}
		memcpy (hostname + hostofs, data + dataofs + 1, eltlen);
		hostname [hostofs + eltlen] = '.';
		hostofs += eltlen + 1;
		dataofs += eltlen + 1;
	}
	//
	// We should not return an empty string
	if (hostofs == 0) {
		return false;
	} else {
		hostname [hostofs-1] = 0;
		return true;
	}
}

typedef bool (*iter_callback_t)(struct sockaddr_storage *ss, socklen_t salen, char* hostname, int port, void *user);

#define UB_CHECK_RESULT(result) (!result->havedata || result->bogus || result->nxdomain)

bool iter_ipv4(struct ub_result *a, char* hostname, int port, iter_callback_t iter_callback, void *user)
{
	struct sockaddr_storage ss;
	memset(&ss, 0, sizeof(ss));
	bool rc = false;
	assert (a != NULL);
	if (UB_CHECK_RESULT(a)) {
		log_errno("RR A: ub result check fails");
	} else {
		for (int i = 0; !rc && a->data[i]; i++) {
			/* Copy IPv4 address and port */
			((struct sockaddr *) &ss)->sa_family = AF_INET;
			memcpy (&((struct sockaddr_in *) &ss)->sin_addr, a->data[i], 4);
			((struct sockaddr_in *) &ss)->sin_port = port;

			rc = iter_callback(&ss, sizeof (struct sockaddr_in), hostname, port, user);
		}
	}
	return rc;
}

bool iter_ipv6(struct ub_result *aaaa, char* hostname, int port, iter_callback_t iter_callback, void *user)
{
	struct sockaddr_storage ss;
	memset(&ss, 0, sizeof(ss));

	bool rc = false;
	assert (aaaa != NULL);
	if (UB_CHECK_RESULT(aaaa)) {
		log_errno("RR AAAA: ub result check fails");
	} else {
		for (int i = 0; !rc && aaaa->data[i]; i++) {
			/* Copy IPv6 address and port */
			((struct sockaddr *) &ss)->sa_family = AF_INET6;
			memcpy (&((struct sockaddr_in6 *) &ss)->sin6_addr, aaaa->data[i], 16);
			((struct sockaddr_in6 *) &ss)->sin6_port = port;
			rc = iter_callback(&ss, sizeof (struct sockaddr_in6), hostname, port, user);
		}
	}
	return rc;
}

bool iter_srv(char *srvname, iter_callback_t iter_callback, void *user)
{
	bool rc = false;
	//
	// Lookup the SRV record to find one or more host/port pairs
	struct ub_result *srv;
	log_debug ("Resolving IN SRV for %s", srvname);
	if (ub_resolve (ubctx, srvname, RR_SRV, CL_IN, &srv) != 0) {
		log_errno ("ub_resolve");
	} else {
		assert (srv != NULL);
		if (UB_CHECK_RESULT(srv)) {
			log_errno ("RR SRV: ub result check fails");
		} else {
			for (int iter_srv = 0; srv->data[iter_srv]; iter_srv++) {
				net_uint16_t netport;
				char hostname [HOSTNAME_BUFSIZE];
				if (_dns2hostport (srv->data [iter_srv], srv->len [iter_srv], hostname, &netport)) {
					log_debug("Following SRV #%d to %s:%d", iter_srv, hostname, ntohs (netport));
					//
					// Find the IP addresses for the host name in the SRV record
					struct ub_result *aaaa;
					log_debug("Resolving IN AAAA for %s", hostname);
					if (ub_resolve (ubctx, hostname, RR_AAAA, CL_IN, &aaaa) != 0) {
						/* This should never fail */
						log_errno("ub_resolve != 0");
					} else {
						rc = iter_ipv6(aaaa, hostname, netport, iter_callback, user);
						ub_resolve_free (aaaa);
						if (rc) {
							break;
						}
					}
					struct ub_result *a;
					log_debug("Resolving IN A for %s", hostname);
					if (ub_resolve (ubctx, hostname, RR_A, CL_IN, &a) != 0) {
						/* This should never fail */
						log_errno("ub_resolve != 0");
					} else {
						rc = iter_ipv4(a, hostname, netport, iter_callback, user);
						ub_resolve_free (a);
						if (rc) {
							break;
						}
					}
				}
			}
		}
		ub_resolve_free(srv);
	}
	return rc;
}

// https://jameshfisher.com/2017/02/28/tcp-server-pthreads/
static int guard(int r, char * err){
	if (r == -1) {
		perror(err);
		exit(1);
	}
	return r;
}

static const derwalk pack_diameter_open[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Open_Request,
	DER_PACK_END
};

static const derwalk pack_diameter_aar[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Authn_Request,
	DER_PACK_END
};

void add_cb(struct peer_info *pi, void *data) {
	if (pi != NULL) {
		state_t *master_state = (state_t *) data;
		log_debug ("CONNECTED TO %s, realm: %s", pi->pi_diamid, pi->config.pic_realm);
		diasasl_send_message_der(master_state);	
	} else {
		log_debug ("add_cb called by fd_peer_free");
	}
}

static bool iter_callback(struct sockaddr_storage *ss, socklen_t salen, char* hostname, int port, void *user)
{
	struct peer_info fddpi;
	state_t *master_state = (state_t *) user;
	memset(&fddpi, 0, sizeof(fddpi));
	fddpi.config.pic_flags.persist = PI_PRST_ALWAYS;
	fd_list_init( &fddpi.pi_endpoints, NULL );
	fddpi.pi_diamid = hostname;
	fddpi.config.pic_port = ntohs (port);
	fddpi.config.pic_realm = master_state->dest_realm;

	CHECK_FCT_DO(fd_ep_add_merge(&fddpi.pi_endpoints, (struct sockaddr *) ss, salen, EP_FL_CONF | EP_ACCEPTALL ), { log_debug("Error in fd_ep_add_merge"); return false; } );
	struct peer_hdr *peer;
	CHECK_FCT_DO(fd_peer_getbyid(hostname, strlen(hostname), 0, &peer), { log_debug("Error getting ConnectPeer information"); return false; } );
	if (peer == NULL) {
		CHECK_FCT_DO(fd_peer_add( &fddpi, "henri", add_cb, master_state ), { log_debug("Error adding ConnectPeer information"); return false; } );
	} else {
		log_debug ("already CONNECTED TO %s, realm: %s", peer->info.pi_diamid, peer->info.config.pic_realm);
		diasasl_send_message_der(master_state);	
	}
	return true;
}

static void lookup_srv(state_t *master_state)
{
	//
	// Construct the SRV query name
	char srvname [HOSTNAME_BUFSIZE];

	snprintf(srvname, sizeof(srvname), "_diameters._sctp.%s", master_state->dest_realm);
	if (!iter_srv(srvname, iter_callback, master_state))
	{
		log_errno ("ran out of SRV RR");
	}
}

static void *handle_connection_der(void * arg)
{
	state_t *master_state = NULL;
	TRACE_DEBUG(INFO, "started thread, socket: %d", (int) (intptr_t) arg);
	if (dermem_open(sizeof (state_t), (void **) &master_state)) {
		TRACE_DEBUG(INFO, "master: %p", master_state);
		master_state->socket = (int) (intptr_t) arg;
		membuf service_realm;
		while (recv_request (master_state, master_state->socket, &master_state->request))
		{
			SASL_DIAMETER_OPEN *diameter_open = &master_state->request.open_request;
			SASL_DIAMETER_AAR *diameter_aar = &master_state->request.authn_request;
			if (!der_isnull (&diameter_open->service_realm)) {
				TRACE_DEBUG(INFO, "Diameter open request");
				service_realm.bufptr = diameter_open->service_realm.derptr;
				service_realm.buflen = diameter_open->service_realm.derlen;
				TRACE_DEBUG(INFO, "diameter_open.service_realm: %.*s", service_realm.buflen, service_realm.bufptr);
				diasasl_send_message_der_open(master_state, diameter_open);
			} else if (!der_isnull (&diameter_aar->session_id)) {
				TRACE_DEBUG(INFO, "Diameter authentication request");
				char *dest_realm = NULL;
				uint32_t dest_realm_len = 0;

				TRACE_DEBUG(INFO, "diameter_aar->session_id: %.*s", diameter_aar->session_id.derlen, diameter_aar->session_id.derptr);
				if (diameter_aar->sasl_mechanism.derptr) {
					char *mech_name = NULL;
					dermem_strdup (master_state, diameter_aar->sasl_mechanism, &mech_name);
					TRACE_DEBUG(INFO, "diameter_aar->sasl_mechanism: %s", mech_name);
					char *cbtyp;
					uint32_t cbtyplen;
					bool fit4plain;

					diasasl_mechinfo(
						mech_name,
						diameter_aar->sasl_token.derptr, diameter_aar->sasl_token.derlen,
						&dest_realm, &dest_realm_len,
						&cbtyp, &cbtyplen,
						&fit4plain
					);
					if (dest_realm == NULL) {
						TRACE_DEBUG(INFO, "no sxover -> realm: %.*s", service_realm.buflen, service_realm.bufptr);
						master_state->dest_realm = NULL;
						mem_strdup (master_state, service_realm, &master_state->dest_realm);
						diasasl_send_message_der(master_state);
					} else {
						TRACE_DEBUG(INFO, "dest_realm: %.*s", dest_realm_len, dest_realm);
						TRACE_DEBUG(INFO, "callback type: %.*s", cbtyplen, cbtyp);
						TRACE_DEBUG(INFO, "fit4plain: %d", fit4plain);
						membuf realm_buf = {
							dest_realm,
							dest_realm_len
						};
						master_state->dest_realm = NULL;
						mem_strdup (master_state, realm_buf, &master_state->dest_realm);
						// SXOVER realm same as server realm?
						if (dest_realm_len == service_realm.buflen && memcmp(service_realm.bufptr, dest_realm, dest_realm_len) == 0) {
							diasasl_send_message_der(master_state);
						} else {
							lookup_srv(master_state);
						}
					}
					if (diameter_aar->sasl_channel_binding.derptr) {
						TRACE_DEBUG(INFO, "diameter_aar->sasl_channel_binding: %.*s", diameter_aar->sasl_channel_binding.derlen, diameter_aar->sasl_channel_binding.derptr);
					}
				} else {
					diasasl_send_message_der(master_state);
				}
			} else if (!der_isnull (&master_state->request.close_request.session_id)) {
				TRACE_DEBUG(INFO, "Diameter close request");
			}
		}
		dermem_close((void **) &master_state);
	} else {
		TRACE_DEBUG(INFO, "error in dermem_open\n");
	}

	TRACE_DEBUG(INFO, "exiting thread, socket: %d", (int) (intptr_t) arg);
	return NULL;
}

void *diasasl_socket_der(void *arg)
{
	char *server_ip = diasasl_conf->diasasl_ip;
	char server_port[10];
	struct sockaddr_storage ss;
	int sock;

	snprintf(server_port, sizeof(server_port), "%d", diasasl_conf->diasasl_port);
	fd_log_debug("server_ip: %s, server_port: %s", server_ip, server_port);
	memset(&ss, 0, sizeof (ss));
	if (socket_parse(server_ip, server_port, &ss)) {
		if (socket_server (&ss, SOCK_STREAM, &sock)) {
			for (;;)
			{
				fd_log_debug("waiting for accept");
				intptr_t conn_fd = guard(accept(sock, NULL, NULL), "Could not accept");
				fd_log_debug("socket accepted: %d", conn_fd);
				pthread_t thread_id;
				int ret = pthread_create(&thread_id, NULL, handle_connection_der, (void*) conn_fd);
				if (ret != 0) {
					printf("Error from pthread: %d\n", ret);
					exit(1);
				}
			}
		} else {
			TRACE_DEBUG(INFO, "Failed to serve network");
		}
	} else {
		TRACE_DEBUG(INFO, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
	}
	pthread_exit(NULL);
	return NULL;
}

static const derwalk pack_diameter_aaa[] = {
	DER_PACK_Quick_DiaSASL_DiaSASL_Answer,
	DER_PACK_END
};

/* Cb called when an answer is received */
static void diasasl_cb_open_ans_der(void * data, struct msg ** msg)
{
	struct timespec ts;
	struct avp * avp;
	struct avp_hdr * hdr;
	int32_t result_code;
	struct session *sess;
	os0_t sid;
	size_t sidlen;
	int *master_state = (int *) data;
	SASL_DIAMETER_AAA diameter_aaa;
	DER_OVLY_Quick_DiaSASL_DiaSASL_Open_Answer *open_answer = &diameter_aaa.open_answer;

	memset (&diameter_aaa, 0, sizeof (diameter_aaa));

	CHECK_FCT_DO( fd_msg_sess_get(fd_g_config->cnf_dict, *msg, &sess, NULL), return );
	fd_log_debug("fd_msg_sess_get sess: %p", sess);

	/* Session-Id */
	if (sess != NULL) {
		CHECK_FCT_DO( fd_sess_getsid( sess, &sid, &sidlen), return );
		open_answer->session_id.derptr = sid;
		open_answer->session_id.derlen = sidlen;
	}

	/* Check the sasl_mechanism Payload AVP */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_avp_sasl_mechanism, &avp), return );
	if (avp) {
		struct avp_hdr * hdr;

		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return  );
		open_answer->sasl_mechanisms.derptr = (hdr->avp_value->os.data);
		open_answer->sasl_mechanisms.derlen = (hdr->avp_value->os.len);
		fd_log_debug("Found sasl_mechanism Payload AVP, mechs: %.*s", open_answer->sasl_mechanisms.derlen, open_answer->sasl_mechanisms.derptr);
	} else {
		fd_log_debug("sasl_mechanism Payload AVP not present");
	}

	/* Value of Origin-Host */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_origin_host, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fd_log_debug("From '%.*s' ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fd_log_debug("no_Origin-Host");
	}

	/* Value of Origin-Realm */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_origin_realm, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fd_log_debug("('%.*s') ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
		open_answer->service_realm.derptr = (hdr->avp_value->os.data);
		open_answer->service_realm.derlen = (hdr->avp_value->os.len);
	} else {
		fd_log_debug("no_Origin-Realm");
	}

	/* Value of Result Code */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_res_code, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		result_code = hdr->avp_value->i32;
		fd_log_debug("Status: %d ", result_code);
	} else {
		fd_log_debug("no_Result-Code ");
	}

	fd_log_debug("sending on socket %d, master %p", *master_state, master_state);
	if (dermem_send (data, *master_state, pack_diameter_aaa, (const dercursor *) &diameter_aaa)) {
		fd_log_debug("dermem_send: Success");
	} else {
		fd_log_debug("dermem_send: FAIL");
	}

	/* Free the message */
	CHECK_FCT_DO(fd_msg_free(*msg), return);
	*msg = NULL;

	return;
}

/* Cb called when an answer is received */
static void diasasl_cb_ans_der(void * data, struct msg ** msg)
{
	struct timespec ts;
	struct avp * avp;
	struct avp_hdr * hdr;
	int32_t result_code;
	struct session *sess;
	os0_t sid;
	size_t sidlen;
	int *master_state = (int *) data;
	SASL_DIAMETER_AAA diameter_aaa;
	DER_OVLY_Quick_DiaSASL_DiaSASL_Authn_Answer *authn_answer = &diameter_aaa.authn_answer;

	memset (&diameter_aaa, 0, sizeof (diameter_aaa));

	CHECK_FCT_DO( fd_msg_sess_get(fd_g_config->cnf_dict, *msg, &sess, NULL), return );
	fd_log_debug("fd_msg_sess_get sess: %p", sess);

	/* Session-Id */
	if (sess != NULL) {
		CHECK_FCT_DO( fd_sess_getsid( sess, &sid, &sidlen), return );
		authn_answer->session_id.derptr = sid;
		authn_answer->session_id.derlen = sidlen;
	}
	/* Check the SASL-Token AVP */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_avp_sasl_token, &avp), return );
	if (avp) {
		struct avp_hdr * hdr;

		fd_log_debug("Found SASL-Token AVP");
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return  );
		authn_answer->sasl_token.derptr      =        (hdr->avp_value->os.data);
		authn_answer->sasl_token.derlen      =        (hdr->avp_value->os.len);
	} else {
		fd_log_debug("SASL-Token AVP *not* found");
	}
	/* Value of Origin-Host */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_origin_host, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fd_log_debug("From '%.*s' ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fd_log_debug("no_Origin-Host");
	}

	/* Value of Origin-Realm */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_origin_realm, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fd_log_debug("('%.*s') ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
		authn_answer->client_domain.derptr = (hdr->avp_value->os.data);
		authn_answer->client_domain.derlen = (hdr->avp_value->os.len);
	} else {
		fd_log_debug("no_Origin-Realm");
	}
	/* Value of Result Code */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_res_code, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		result_code = hdr->avp_value->i32;
		fd_log_debug("Status: %d ", result_code);
	} else {
		fd_log_debug("no_Result-Code ");
	}

	/* Check the User-Name AVP */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diasasl_user_name, &avp), return );
	if (avp) {
		struct avp_hdr * hdr;

		fd_log_debug("Found User-Name AVP");
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return  );
		authn_answer->client_userid.derptr      =        (hdr->avp_value->os.data);
		authn_answer->client_userid.derlen      =        (hdr->avp_value->os.len);
	} else {
		fd_log_debug("User-Name AVP *not* found");
	}
	der_buf_int32_t buf;
	if (result_code == 2001) {
		authn_answer->final_comerr = der_put_int32 (buf, 0); // success
	}

	fd_log_debug("sending on socket %d, master %p", *master_state, master_state);
	if (dermem_send (data, *master_state, pack_diameter_aaa, (const dercursor *) &diameter_aaa)) {
		fd_log_debug("dermem_send: Success");
	} else {
		fd_log_debug("dermem_send: FAIL");
	}

	/* Free the message */
	CHECK_FCT_DO(fd_msg_free(*msg), return);
	*msg = NULL;

	return;
}

static void diasasl_send_message_der_open(state_t *master_state, SASL_DIAMETER_OPEN *diameter_open)
{
	struct msg * req = NULL;
	struct avp * avp;
	union avp_value val;

	/* Create the request */
	CHECK_FCT_DO( fd_msg_new( diasasl_cmd_r, MSGFL_ALLOC_ETEID, &req ), goto out );

	struct session *session = NULL;

	/* Now set all AVPs values */

	/* Session-Id */
	/* Create a new session */
	#define DIASASL_CLI_SID_OPT  "diasasl_cli"
	CHECK_FCT_DO( fd_msg_new_session( req, (os0_t)DIASASL_CLI_SID_OPT, CONSTSTRLEN(DIASASL_CLI_SID_OPT) ), goto out );
	fd_log_debug("diasasl_send_message_der_open: creating new session");

	/* Set the Destination-Realm AVP */
	{
		/* Use server realm */
		val.os.data = (unsigned char *)(diasasl_conf->dest_realm);
		val.os.len  = strlen(diasasl_conf->dest_realm);

		fd_log_debug("diasasl_send_message_der_open: dest_realm: %.*s", val.os.len, val.os.data);
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_dest_realm, 0, &avp ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set the Destination-Host AVP if needed*/
	if (diasasl_conf->dest_host) {
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_dest_host, 0, &avp ), goto out  );
		val.os.data = (unsigned char *)(diasasl_conf->dest_host);
		val.os.len  = strlen(diasasl_conf->dest_host);
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set Origin-Host & Origin-Realm */
	CHECK_FCT_DO( fd_msg_add_origin ( req, 0 ), goto out  );

	/* Set the User-Name AVP if needed*/
	if (diasasl_conf->user_name) {
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_user_name, 0, &avp ), goto out  );
		val.os.data = (unsigned char *)(diasasl_conf->user_name);
		val.os.len  = strlen(diasasl_conf->user_name);
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set the SASL-Mechanism AVP */
	{
		/* Mechanisms request -> empty mechlist */
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_avp_sasl_mechanism, 0, &avp ), goto out  );
		val.os.data = "";
		val.os.len = 0;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Send the request */
	fd_log_debug("sending on socket %d, master %p", master_state->socket, master_state);
	CHECK_FCT_DO( fd_msg_send( &req, diasasl_cb_open_ans_der, &master_state->socket ), goto out );

out:
	return;
}

static void diasasl_send_message_der(state_t *master_state)
{
	struct msg * req = NULL;
	struct avp * avp;
	union avp_value val;
	
	SASL_DIAMETER_AAR *diameter_aar = &master_state->request.authn_request;

	/* Create the request */
	CHECK_FCT_DO( fd_msg_new( diasasl_cmd_r, MSGFL_ALLOC_ETEID, &req ), goto out );

	struct session *session = NULL;

	/* Now set all AVPs values */

	/* Session-Id */
	if (diameter_aar->session_id.derptr == NULL) {
		/* Create a new session */
		#define DIASASL_CLI_SID_OPT  "diasasl_cli"
		CHECK_FCT_DO( fd_msg_new_session( req, (os0_t)DIASASL_CLI_SID_OPT, CONSTSTRLEN(DIASASL_CLI_SID_OPT) ), goto out );
		fd_log_debug("creating new session");
	} else {
		fd_log_debug("using existing session: %.*s", diameter_aar->session_id.derlen, diameter_aar->session_id.derptr);
		/* get session */
		CHECK_FCT_DO( fd_sess_fromsid (diameter_aar->session_id.derptr, diameter_aar->session_id.derlen, &session, NULL), goto out );
		/* Create an AVP to hold it */
		CHECK_FCT_DO( fd_msg_avp_new( diasasl_sess_id, 0, &avp ), goto out );

		/* Set its value */
		memset(&val, 0, sizeof(val));
		val.os.data = diameter_aar->session_id.derptr;
		val.os.len  = diameter_aar->session_id.derlen;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out );

		/* Add it to the message */
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_FIRST_CHILD, avp ), goto out ); 
	}

	/* Set the Destination-Realm AVP */
	{
		struct sess_state *realm_state = NULL;
		if (session == NULL) {
			fd_log_debug("No session yet");
		} else {
			/* Search the session, retrieve its data */
			fd_log_debug("retrieving session state");
			/* retrieve this value in the session, AFTERWARDS it is removed from the list! */
			CHECK_FCT_DO( fd_sess_state_retrieve( sess_handler, session, &realm_state ), goto out );
			fd_log_debug("retrieved session state: %p", realm_state);
		}
		if (realm_state == NULL) {
			if (master_state->dest_realm != NULL) {
				realm_state = malloc(sizeof(struct sess_state));
				if (realm_state == NULL) {
					fd_log_debug("malloc failed: %s", strerror(errno));
					goto out;
				}
				strcpy(realm_state->dest_realm, master_state->dest_realm);
				fd_log_debug("setting dest_realm: %s", realm_state->dest_realm);
				/* Use client realm */
				val.os.data = (unsigned char *) realm_state->dest_realm;
				val.os.len  = strlen(realm_state->dest_realm);
			} else {
				/* Use server realm */
				val.os.data = (unsigned char *)(diasasl_conf->dest_realm);
				val.os.len  = strlen(diasasl_conf->dest_realm);
			}
		} else {
			/* Use client realm */
			val.os.data = (unsigned char *) realm_state->dest_realm;
			val.os.len  = strlen(realm_state->dest_realm);
		}
		fd_log_debug("dest_realm: %.*s", val.os.len, val.os.data);
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_dest_realm, 0, &avp ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
		if (session != NULL) {
			/* (re)store this value in the session, AFTERWARDS realm_state is set to NULL! (changed ownership) */
			CHECK_FCT_DO( fd_sess_state_store ( sess_handler, session, &realm_state ), goto out );
		}
	}

	/* Set the Destination-Host AVP if needed*/
	if (diasasl_conf->dest_host) {
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_dest_host, 0, &avp ), goto out  );
		val.os.data = (unsigned char *)(diasasl_conf->dest_host);
		val.os.len  = strlen(diasasl_conf->dest_host);
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set Origin-Host & Origin-Realm */
	CHECK_FCT_DO( fd_msg_add_origin ( req, 0 ), goto out  );

	/* Set the User-Name AVP if needed*/
	if (diasasl_conf->user_name) {
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_user_name, 0, &avp ), goto out  );
		val.os.data = (unsigned char *)(diasasl_conf->user_name);
		val.os.len  = strlen(diasasl_conf->user_name);
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set the SASL-Mechanism AVP */
	if (diameter_aar->sasl_mechanism.derptr != NULL)
	{
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_avp_sasl_mechanism, 0, &avp ), goto out  );
		val.os.data = diameter_aar->sasl_mechanism.derptr;
		val.os.len = diameter_aar->sasl_mechanism.derlen;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set the SASL-Channel-Binding AVP */
	if (diameter_aar->sasl_channel_binding.derptr != NULL)
	{
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_avp_sasl_channel_binding, 0, &avp ), goto out  );
		val.os.data = diameter_aar->sasl_channel_binding.derptr;
		val.os.len = diameter_aar->sasl_channel_binding.derlen;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Set the SASL-Token AVP */
	if (diameter_aar->sasl_token.derptr != NULL)
	{
		CHECK_FCT_DO( fd_msg_avp_new ( diasasl_avp_sasl_token, 0, &avp ), goto out  );
		val.os.data = diameter_aar->sasl_token.derptr;
		val.os.len = diameter_aar->sasl_token.derlen;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}

	/* Send the request */
	fd_log_debug("sending on socket %d, master %p", master_state->socket, master_state);
	CHECK_FCT_DO( fd_msg_send( &req, diasasl_cb_ans_der, &master_state->socket ), goto out );

out:
	return;
}

int diasasl_cli_init(void)
{
	CHECK_FCT( fd_sess_handler_create(&sess_handler, (void *)free, NULL, NULL) );

	int rc = -1;
	assert (ubctx == NULL);
	//
	// Open the Unbound library
	ubctx = ub_ctx_create();
	if (ubctx == NULL) {
		log_errno ("ub_ctx_create()");
	} else {
		//
		// Resolve in a thread rather than a process
		if (ub_ctx_async(ubctx, 1) != 0) {
			log_errno("ub_ctx_async()");
		} else {
			//
			// Load the hosts file, can be overridden with envvar UNBOUND_HOSTS
			char *opt_hosts = getenv ("UNBOUND_HOSTS");
			if (opt_hosts == NULL) {
#ifndef _WIN32
					opt_hosts = "/etc/hosts";
#else
					char *system_root;
					assert((system_root = getenv ("SystemRoot")) != NULL);
					static char default_hosts[256];
					if (snprintf(default_hosts, sizeof(default_hosts), "%s\\System32\\drivers\\etc\\hosts", system_root) < sizeof(default_hosts)) {
						opt_hosts = default_hosts;
					}
#endif
			}
			log_debug ("opt_hosts=%s", opt_hosts);
			if (ub_ctx_hosts (ubctx, opt_hosts) != 0) {
				log_errno ("ub_ctx_hosts");
			} else {
				rc = 0;
			}
		}
	}
	return rc;
}

void diasasl_cli_fini(void)
{
	// CHECK_FCT_DO( fd_sig_unregister(diasasl_conf->signal), /* continue */ );
	assert(ubctx != NULL);
	ub_ctx_delete(ubctx);
	ubctx = NULL;
	CHECK_FCT_DO( fd_sess_handler_destroy(&sess_handler, NULL), /* continue */ );
	return;
};
