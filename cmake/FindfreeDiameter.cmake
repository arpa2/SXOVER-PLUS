# FindfreeDiameter -- Locate freeDiameter resources
#
# This module locates the include directory and the daemon program.
#
# Usage example:
#    find_package (freeDiameter 1.2.0 REQUIRED)
#
# Normally, this module exports:
#  - freeDiameter_FOUND
#  - freeDiameter_LIBRARIES
#  - freeDiameter_INCLUDE_DIRS
#  - freeDiameter_VERSION defaults to "UNKNOWN"
#  - freeDiameter_EXTENSION_DIR
#
# In addition, if the freeDiameterd daemon is found:
#  - freeDiameter_EXECUTABLE is set to the daemon path
#  - freeDiameter_VERSION    is set to the daemon version
#
# The module also provides a rule to define a new extension,
#  - add_freeDiameter_extension (EXTNAME [EXCLUDE_FROM_ALL] SOURCE...)
#
#    This macro adds a new (shared) library for use as a freeDiameter plugin,
#    but freeDiameter plugin-library naming is non-standard: there is no "lib"
#    prefix, and they use the extension ".fdx" instead of ".so"
#
#    Extensions install into ${freeDiameter_EXTENSION_DIR} for both their
#    LIBRARY and RUNTIME DESTINATION, so the target (EXTNAME) should
#    have a custom install command as well:
#
#    ```
#    install (...
#        LIBRARY DESTINATION ${freeDiameter_EXTENSION_DIR}
#        RUNTIME DESTINATION ${freeDiameter_EXTENSION_DIR}
#    )
#    ```
#
#
# From: Rick van Rein <rick@openfortress.nl>


include(FindPackageHandleStandardArgs)

find_library (freeDiameter_fdcore_LIBRARY  fdcore
	PATHS $ENV{DESTDIR_freediameter}/usr $ENV{DESTDIR_freediameter}/usr/local $ENV{CMAKE_SYSTEM_PREFIX_PATH} ${CMAKE_PREFIX_PATH}
	PATH_SUFFIXES "lib")
find_library (freeDiameter_fdproto_LIBRARY fdproto
	PATHS $ENV{DESTDIR_freediameter}/usr $ENV{DESTDIR_freediameter}/usr/local $ENV{CMAKE_SYSTEM_PREFIX_PATH} ${CMAKE_PREFIX_PATH}
	PATH_SUFFIXES "lib")

find_path (freeDiameter_EXTENSION_DIR
    dict_nasreq.fdx
    PATHS $ENV{DESTDIR_freediameter}/usr $ENV{DESTDIR_freediameter}/usr/local
    ### BUG IN CMAKE??  It adds /bin/ to the ${CMAKE_SYSTEM_PREFIX_PATH} so we need a ../ variant path...
    PATH_SUFFIXES lib/freeDiameter ../lib/freeDiameter
)
find_path (freeDiameter_INCLUDE_DIR
    libfdcore.h
    PATHS $ENV{DESTDIR_freediameter}/usr $ENV{DESTDIR_freediameter}/usr/local ${CMAKE_INCLUDE_PATH} ${CMAKE_SYSTEM_INCLUDE_PATH}
    # NO_DEFAULT_PATH
    ### BUG IN CMAKE??  It adds /bin/ to the ${CMAKE_SYSTEM_PREFIX_PATH} so we need a ../ variant path...
    PATH_SUFFIXES freeDiameter include/freeDiameter ../include/freeDiameter
)

find_program (freeDiameter_EXECUTABLE
    freeDiameterd
    PATHS $ENV{DESTDIR_freediameter}/usr/bin $ENV{DESTDIR_freediameter}/usr/sbin $ENV{DESTDIR_freediameter}/usr/local/bin $ENV{DESTDIR_freediameter}/usr/local/sbin)

# Munge the path if it is found
if (freeDiameter_INCLUDE_DIR)
    string (REGEX REPLACE "[/\\]freeDiameter$" ""
        freeDiameter_INCLUDE_DIR
        "${freeDiameter_INCLUDE_DIR}")
endif ()

if (freeDiameter_EXECUTABLE)
    execute_process (COMMAND ${freeDiameter_EXECUTABLE} --version
        OUTPUT_VARIABLE freeDiameter_VERSION_OUTPUT)
    if ("${freeDiameter_VERSION_OUTPUT}" MATCHES "^.*version *([^ \n]+)")
        set (freeDiameter_VERSION "${CMAKE_MATCH_1}")
    else ()
        set (freeDiameter_VERSION "UNKNOWN")
    endif ()
endif ()

find_package_handle_standard_args (freeDiameter
    REQUIRED_VARS freeDiameter_fdcore_LIBRARY
                  freeDiameter_fdproto_LIBRARY
                  freeDiameter_INCLUDE_DIR
                  freeDiameter_EXTENSION_DIR
    VERSION_VAR freeDiameter_VERSION
)

if (freeDiameter_FOUND)
    message (STATUS "Found freeDiameter version ${freeDiameter_VERSION}")
    set (freeDiameter_LIBRARIES
        ${freeDiameter_fdcore_LIBRARY}
        ${freeDiameter_fdproto_LIBRARY})
    set (freeDiameter_INCLUDE_DIRS
        ${freeDiameter_INCLUDE_DIR})
    add_library (freeDiameter::fdcore  UNKNOWN IMPORTED)
    add_library (freeDiameter::fdproto UNKNOWN IMPORTED)
    set_property (TARGET freeDiameter::fdcore
        PROPERTY IMPORTED_LOCATION "${freeDiameter_fdcore_LIBRARY}")
    set_property (TARGET freeDiameter::fdproto
        PROPERTY IMPORTED_LOCATION "${freeDiameter_fdproto_LIBRARY}")
    set_property (TARGET freeDiameter::fdcore
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${freeDiameter_INCLUDE_DIR}")
    set_property (TARGET freeDiameter::fdproto
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${freeDiameter_INCLUDE_DIR}")
endif ()

macro (add_freeDiameter_extension EXTNAME)
    add_library ("${EXTNAME}" MODULE ${ARGN})
    set_target_properties ("${EXTNAME}" PROPERTIES PREFIX ""    )
    set_target_properties ("${EXTNAME}" PROPERTIES SUFFIX ".fdx")
endmacro (add_freeDiameter_extension)

