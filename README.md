# SASL code for the SXOVER-PLUS mechanism for Realm Crossover

> *Realm Crossover was invented by InternetWide.org to enable any domain
> owner to define user identities that look like email addresses and that
> can be authenticated by any other realm.  This is a SASL mechanism for
> achieving this effect.*

Realm Crossover is a
[general idea](https://datatracker.ietf.org/doc/html/draft-vanrein-internetwide-realm-crossover)
for allowing domain owners to offer user authentication services to
other domains.  The
[general identity form](http://common.arpa2.net/md_doc_IDENTITY.html)
can be generalised as
[selector identities](http://common.arpa2.net/md_doc_IDENTITY.html)
that sites may use in any way they like;
the InternetWide approach is to use
[generic policy rules](http://common.arpa2.net/md_doc_RULES.html)
for applications like
[Communication Access](http://common.arpa2.net/md_doc_ACCESS_COMM.html)
or
[Document Access](http://common.arpa2.net/md_doc_ACCESS_DOCUMENT.html)
with advanced support for
[Group Membership](http://common.arpa2.net/md_doc_GROUP.html)
and other kinds of
[Acting-on-behalf-of](http://common.arpa2.net/md_doc_ACTOR.html).

## Pictures beat a Thousand Words

The
[protocol draft](https://tools.ietf.org/id/draft-vanrein-diameter-sasl)
provides this useful image

```
   +--------+    SASL     +--------+    SASL    +---------+
   | Client |-----------> | Server | ---------> |  Realm  |
   +--------+  AppProto   +--------+  Diameter  +---------+
       ||                     ||                    ||
john@example.com        find SRV, TLSA          example.com
  & credential            relay SASL           authentication
```

where `Realm` is the client's home realm, and `Server` is a foreign
server; foreign to the client, that is.  The `SXOVER-PLUS` mechanism
wraps SASL as part of an application protocol `AppProto` and this is
forward to the server for the client's `Realm`.

Realms are a security naming habit, but under `SXOVER-PLUS` it is
the same as the domain part `example.com` of the client's
`john@example.com` identity.


## Realm Crossover for SASL

The SASL variant of Realm Crossover can be realised with the `SXOVER-PLUS`
mechanism, which involves:

  * End-to-end encryption between the client and their home realm
  * Passing this tunnel traffic to a foreign server
  * Relaying the tunnel traffic to the home realm

The implementation of this mechanism is through extensions for existing
SASL solutions, found in various `contrib` subdirectories.  If your SASL
library is not there yet, we would love to see your Merge Request!

The dominant SASL implementation is Cyrus-SASL2.  This is the least we
shall provide in this project.  This code originated as an API-compliant
variation on
[Quick-SASL](http://quick-sasl.arpa2.net/),
a simplified wrapper around any-SASL-but-often-Cyrus.


## Technologies used

The encryption tunnel is founded on the ARPA2 encryption service protocol
for **Keyful Identity Protocol** based on the
[KIP Library](http://kip.arpa2.net/).
This protocol is novel in its own right; without knowing a recipient's
public key, it is possible to encrypt content.  It is up to the
recipient to decrypt it by authenticating to the KIP Service and
retrieve a session key in return.  This means that encrypted content
can be pushed to anyone.  This mechanism is used to construct an
end-to-end encrypted tunnel for the `SXOVER-PLUS` mechanism.
KIP itself relies on Kerberos encryption primitives.
The normal behaviour is to obtain a session key for a recipient at
domain KIP service for the recipient's domain.

To relay traffic to the client's realm, the best protocol is
[Diameter](https://datatracker.ietf.org/doc/html/rfc6733)
with the
[Network Access Service](https://datatracker.ietf.org/doc/html/rfc7155)
which one might trivialise as a cross-realm extension of RADIUS.
It really is the most scalable, secure and reliable protocol for this
purpose, plus it is completely standardised.  The only things we had
to add were a few attributes that transport the SASL interaction.


## Security

The client uses KIP to establish a secret with their home realm.
They might do this as often as they like, for example upon login,
so there never is a need to swap out the secret to disk.  This is
an anonymous action, which requires no password.

The client then connects to a foreign server, chooses SASL authentication
and, when offered `SXOVER-PLUS` authentication, it starts this to
securely wrap a simpler SASL exchange of which the foreign server
is not informed.

The only thing the foreign server sees is the realm (the domain name)
under which the client wants to authenticate.  This is used to connect
to a Diameter server for that domain.  The foreign server relies on
TLS, DANE and DNSSEC to assure that this is the authoritative identity
server for the domain, as stated by its SRV records.  Given that, the
SASL traffic is relayed between the client and its realm with the
foreign server simply transporting it literally.  The identity server
for the client's home realm responds with a username only, to which
the foreign server attaches the previously validated domain to form a
`user@domain.name` style identity for the client.  Effectively, the
domain's identity server is used to release authoritative statements
about the username part of the client identity.

A special note concerns channel binding, which is part of `SXOVER-PLUS`
on account of the `-PLUS` ending.  Channel binding assures that the
connection is indeed made to the foreign server.  This requires the
foreign server to relay their identity, which happens to be secure
in this special case because Diameter does not grant access to any
resources (like a mailbox) that could be abused by the foreign server;
instead, it only hears a qualified yes or no on the client identity.

The foreign server can now use access control mechanisms, locate the
client's existing data, and so on.  The client does not need any
identity local to the server, as has been *in vogue* since web servers
imposed their limited idea of security onto everyday users.  Note that
we designed another protocol named
[HTTP-SASL](https://tools.ietf.org/id/draft-vanrein-httpauth-sasl)
to allow HTTP to grow along with security principles that have been
beneficial for most other protocols.


## Supported Targets

The project always builds a `libxoversasl.so` which is used on a
client or server that speaks `SXOVER-PLUS` on top of any other SASL
mechanism that is reachable through a Quick-SASL library.

The project always builds a `libxoversaslpass.so` that helps proxies
that pass SASL to decide what constitutes suitable mechanisms to
pass to remote parties.  This is a more delicate question than a mere
plaintext-or-TLS distinction, because TLS is unwrapped by a proxy.

The project defaults to `SUPPORT_CYRUS_SASL2` which makes it build a
`libsxoverplus.so` plugin for Cyrus-SASL2.

