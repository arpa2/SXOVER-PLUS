/* A SASL client based on Xover SASL.
 *
 * It differs only on slight details from the qsasl-client, and that is
 * deliberate -- both as a demonstration of the difference, and to be
 * able to test plain Quick SASL against Xover SASL.
 *
 * This is similar to the sasl-sample-client provided with Cyrus SASL2,
 * but we use different prompts and switched from BASE64 to HEX for
 * reasons of human readability (the term "human" is debatable, grinn).
 * When translating, also note the different mechanism lists; these are
 * not standardised as part of SASL, so each makes up their own.
 *
 * The client can connect to an IP and port combination, where a server
 * would be listening.  When this is not requested, it will instead assume
 * traffic on stdin in a somewhat standard format.
 *
 * Lines are prefixed with either "c2s> " or "s2c> " to indicate the kind
 * of information and the traffic passed.  Plus, initially there is a
 * "mech> " prefix from server to client with the mechanism, and one from
 * the client back to the server.  When returning data with success, the
 * prompt "extra> " might also be used.  Any lines not recognised are
 * quietly dropped, except for a message about misrecognised prompts.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>

#include <arpa2/xover-sasl.h>

#include "arpa2/kip.h"


#define DEFAULT_PORT "16832"


#include "sasl-common.c"



/* Main program.
 */
int main (int argc, char *argv []) {
	bool realm_crossover = false;
	char *service = "qsasl-xsasl-demo";
	//
	// Harvest commandline arguments
	assert (argc > 0);
	char *program = argv [0];
	if ((argc >= 2) && (strchr (argv [1], '.') == NULL) && (strchr (argv [1], ':') == NULL)) {
		service = argv [1];
		argv++;
		argc--;
	}
	if (argc > 4) {
		fprintf (stderr, "Usage: %s [serviceName] [serverIP [serverPort [chanbindvalue]]]\n"
				"Default serviceName is qsasl-xsasl-demo, serverPort is " DEFAULT_PORT " but without serverIP, the user is the network\n",
				program);
		exit (1);
	}
	socket_init();
	bool tty_user_networking = (argc < 2);
	char *server_ip   = (argc >= 2) ? argv [1] : NULL        ;
	char *server_port = (argc >= 3) ? argv [2] : DEFAULT_PORT;
	char *chan_bind   = (argc >= 4) ? argv [3] : NULL        ;
	//
	// Have streams for networking
	FILE *s2c_stream = stdin;
	FILE *c2s_stream = stdout;
	if (tty_user_networking) {
		printf ("\n*\n* You did not supply a network.  No problem.\n* Please copy/paste lines to the qsasl-server, xsasl-server or diasasl-proxy.\n*\n\n");
	} else {
		struct sockaddr_storage ss;
		memset (&ss, 0, sizeof (ss));
		if (!socket_parse (server_ip, server_port, &ss)) {
			fprintf (stderr, "Syntax error in host ip:port setting %s:%s\n", server_ip, server_port);
			exit (1);
		}
		int sox;
		if (!socket_client (&ss, SOCK_STREAM, &sox)) {
			perror ("Failed to connect");
			exit (1);
		}
		s2c_stream = fdopen (dup (sox), "r");
		c2s_stream = fdopen (     sox , "w");
	}
	printf ("--\n");
	fflush (stdout);
	//
	// Start the KIP Service
	kip_init ();
	kipservice_init (NULL, NULL);
	//
	// Start the SASL session as a client.
	xsasl_init (NULL, "InternetWide_Identity");
	XoverSASL sasl = NULL;
	xsaslt_state curstate = QSA_UNDECIDED;
	assert (xsasl_client (&sasl, service, cb_statechange, &curstate, 0));
printf ("Constructed XoverSASL client %p\n", sasl);
	//
	// Be someone -- including step down to the ACL user
	char *str_login = getenv ("KIPSERVICE_CLIENTUSER_LOGIN");
	char *str_acl   = getenv ("KIPSERVICE_CLIENTUSER_ACL"  );
	char *str_realm = getenv ("KIPSERVICE_CLIENT_REALM"    );
	char *str_servr = getenv ("KIP_REALM"                  );
	dercursor crs_login = { .derptr=str_login, .derlen=strlen (str_login) };
	dercursor crs_acl   = { .derptr=str_acl  , .derlen=strlen (str_acl  ) };
	dercursor crs_realm = { .derptr=str_realm, .derlen=strlen (str_realm) };
	dercursor crs_servr = { .derptr=str_servr, .derlen=strlen (str_servr) };
	assert (xsasl_set_clientuser_login (sasl, crs2buf (crs_login)));
	assert (xsasl_set_clientuser_acl   (sasl, crs2buf (crs_acl  )));
	assert (xsasl_set_client_realm     (sasl, crs2buf (crs_realm)));
	assert (xsasl_set_server_realm     (sasl, crs2buf (crs_servr)));
	if (chan_bind != NULL) {
		uint8_t *cb = NULL;
		assertxt (xsasl_alloc (sasl, strlen(QSA_X_MANUAL_SESSION_KEY) + strlen(chan_bind) + 2, &cb), "Failed to allocate channel binding value");
		sprintf (cb, "%s:%s", QSA_X_MANUAL_SESSION_KEY, chan_bind);
		membuf mem_chanbind = {
			.bufptr = cb,
			.buflen = strlen (cb)
		};
		assert (xsasl_set_chanbind (sasl, false, mem_chanbind));
	}
	//
	// Now wait for a list of mechanisms from the server
	static const char *only_mech [] = { "mech", NULL };
	const char *lineprompt;
	dercursor mechs;
	prompted_hex_recv (sasl, s2c_stream, only_mech, &lineprompt, &mechs);
	assert (xsasl_set_mech (sasl, mechs));
	//
	// Start looping as a client
	dercursor s2c = {
		.derptr = NULL,
		.derlen = 0
	};
	dercursor mech_used = {
		.derptr = NULL,
		.derlen = 0
	};
	while (curstate == QSA_UNDECIDED) {
		//
		// Make a client step: s2c --> mech, c2s
		dercursor c2s;
		dercursor mech;
		assert (xsasl_step_client (sasl, s2c, &mech, &c2s));
		//
		// If we received "extra" then we can check proper ending
		if (strcmp (lineprompt, "extra") == 0) {
			assert (mech.derptr == NULL);
			assert (curstate != QSA_UNDECIDED);
			break;
		}
		//
		// We optionally send "mech> " before "c2s> "
		if (mech.derptr != NULL) {
			prompted_hex_send (sasl, c2s_stream, "mech", mech);
			mech_used = mech;
		}
		//
		// Send c2s in HEX to the server
		prompted_hex_send (sasl, c2s_stream, "c2s", c2s);
		//
		// TODO: what if curstate != QSA_UNDECIDED
		if (curstate != QSA_UNDECIDED) {
			break;
		}
		//
		// Retrieve the HEX result from the server
		static const char *either_s2c_or_extra []  = { "s2c", "extra", "eof", NULL };
		prompted_hex_recv (sasl, s2c_stream, either_s2c_or_extra, &lineprompt, &s2c);
		//
		// If we received "eof" then SASL was torn down on failure
		if (strcmp (lineprompt, "eof") == 0) {
			assert (curstate != QSA_SUCCESS);
			curstate = QSA_FAILURE;
			break;
		}
	}
	//
	// Print the mechanism used, if it was decided on
	if (mech_used.derptr != NULL) {
		printf ("Mechanism used was %.*s\n", (int) mech_used.derlen, mech_used.derptr);
	}
	//
	// Report whether the exchange was successful
	printf ("\n*\n* The SASL exchange was a %s.  Application %s.\n*\n\n",
			(curstate == QSA_SUCCESS) ? "SUCCESS" : "FAILURE",
			(strcmp (lineprompt, "extra") == 0) ? "on server succeeded" : "will pass server verdict" );
	//
	// Close down sockets
	fclose (c2s_stream);
	fclose (s2c_stream);
	//
	// Cleanup & Closedown
	xsasl_close (&sasl);
	xsasl_fini ();
	kipservice_fini ();
	kip_fini ();
	socket_fini ();
	exit ((curstate == QSA_SUCCESS) ? 0 : 1);
}
